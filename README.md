# OpenML dataset: confidence

https://www.openml.org/d/468

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

CODING:

ITEM 1 = BUSINESS CONDIDIONS 6 MONTHS FROM NOW  (CONFERENCE BOARD)
ITEM 2 = JOBS 6 MONTHS FROM NOW (CONFERENCE BOARD)
ITEM 3 = FAMILY INCOME 6 MONTHS FROM NOW  (CONFERENCE BOARD)
ITEM 4 = BUSINESS CONDITIONS A YEAR FROM NOW  (MICHIGAN)
ITEM 5 = JOBS DURING THE COMING 12 MONTHS  (MICHIGAN)
ITEM 6 = FAMILY INCOME DURING THE NEXT 12 MONTHS  (MICHIGAN)

RESPONSE P = PESSIMISTIC
RESPONSE N = NEUTRAL
RESPONSE O = OPTIMISTIC


1992 DATA:




I AM GORDON BECHTEL AT E-MAIL BECHTEL AT NERVM.NERDC.UFL.EDU
I AM WILLING TO HELP THOSE WHO HAVE PROBLEMS WITH THESE DATA.


(3) THE CONFERENCE BOARD HAS GIVEN ITS PERMISSION TO PLACE ITS THREE
"6 MONTH" ITEMS IN STATLIB.
THE SURVEY RESEARCH CENTER AT THE UNIVERSITY OF MICHIGAN HAS GIVEN
ITS PERMISSION TO PLACE ITS THREE "12 MONTH" ITEMS IN STATLIB.


Information about the dataset
CLASSTYPE: nominal
CLASSINDEX: 1

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/468) of an [OpenML dataset](https://www.openml.org/d/468). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/468/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/468/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/468/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

